<?php

include 'currenturl.php';

?>
<!doctype html>
<html lang="en-gb" class="no-js"> 

<head>
<title>Setutech</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" type="image/png" href="images/fav-2.png"/>



<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>



<link rel="stylesheet" href="css/reset.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />

<link rel="stylesheet" href="css/w3.css" type="text/css" />


<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">


<link rel="stylesheet" type="text/css" href="css/simpleline-icons/simple-line-icons.css" media="screen" />


<link href="js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />


<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />


<link rel="stylesheet" media="screen" href="css/shortcodes.css" type="text/css" /> 




<link href="js/mainmenu/bootstrap.min.css" rel="stylesheet">
<link href="js/mainmenu/menu-2.css" rel="stylesheet">


<link rel="stylesheet" type="text/css" href="js/slidepanel/slidepanel.css">


<link rel="stylesheet" href="js/masterslider/style/masterslider.css" />
<link rel="stylesheet" href="js/masterslider/skins/default/style.css" />


<link href="js/carouselowl/owl.transitions.css" rel="stylesheet">
<link href="js/carouselowl/owl.carousel.css" rel="stylesheet">


<link rel="stylesheet" href="js/iconhoverefs/component.css" />


<link rel="stylesheet" href="js/basicslider/bacslider.css" />


<link rel="stylesheet" type="text/css" href="js/cubeportfolio/cubeportfolio.min.css">


<link rel="stylesheet" href="js/flexslider/flexslider.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="js/flexslider/skin.css" />


<link rel="stylesheet" type="text/css" href="js/tabs/assets/css/responsive-tabs3.css">


<link rel="stylesheet" type="text/css" href="js/accordion/style.css" />



</head>

<body>

<div class="site_wrapper">

<?php include 'includes/header.php' ?>

<div class="clearfix"></div>



<div class="mstslider">


<div class="master-slider" id="masterslider">


<div class="ms-slide slide-1" data-delay="7">


<img src="images/parallax2.png"> 


<div class="ms-layer centext text1 white"
style="top:240px;"
data-effect="bottom(50)"
data-duration="900"
data-delay="300"
data-ease="easeOutExpo"
><strong>CORELDRAW GRAPHICS SUITE 2017</strong></div>

<div class="ms-layer centext text2 white"
style="top:329px;"
data-effect="bottom(50)"
data-duration="2000"
data-delay="500"
data-ease="easeOutExpo"
>Get all of our industry-acclaimed tools found in CorelDRAW Graphics Suite 2017, Plus so much more.</div>

<div class="ms-layer centext sdbut"
style="top:435px;"
data-effect="bottom(100)"
data-duration="2000"
data-delay="900"
data-ease="easeOutExpo"
><a href="/products/coreldraw-graphics-suite-2017.php">Read More</a></div>

</div>


<div class="ms-slide slide-2" data-delay="7">


<img src="images/parallax.png"> 


<div class="ms-layer centext text1 white"
style="top:240px;"
data-effect="bottom(50)"
data-duration="900"
data-delay="300"
data-ease="easeOutExpo"
><strong>AUTODESK AUTOCAD 2018</strong></div>

<div class="ms-layer centext text2 white"
style="top:329px;"
data-effect="bottom(50)"
data-duration="2000"
data-delay="500"
data-ease="easeOutExpo"
>Design every detail with Autodesk AutoCAD software, one of the world's leading CAD applications</div>

<div class="ms-layer centext sdbut"
style="top:435px;"
data-effect="bottom(100)"
data-duration="2000"
data-delay="900"
data-ease="easeOutExpo"
><a href="/products/autodesk-autocad-2018.php">Read More</a></div>



</div>


<div class="ms-slide slide-3" data-delay="7">


<img src="images/home-parallax-section-works.jpg">


<div class="ms-layer centext text1 white"
style="top:240px;"
data-effect="bottom(50)"
data-duration="900"
data-delay="300"
data-ease="easeOutExpo"
><strong>ADOBE CREATIVE CLOUD</strong></div>

<div class="ms-layer centext text2 white"
style="top:329px;"
data-effect="bottom(50)"
data-duration="2000"
data-delay="500"
data-ease="easeOutExpo"
>Edit Photos and Videos. Design graphics and publications. Create websites, UX designs and animations.</div>

<div class="ms-layer centext sdbut"
style="top:435px;"
data-effect="bottom(100)"
data-duration="2000"
data-delay="900"
data-ease="easeOutExpo"
><a href="/products/adobe-creative-cloud.php">Read More</a></div>

</div>

</div>

</div>

<div class="clearfix"></div>
<div class="content_fullwidth">

<div class="features_sec9 three">
<div class="container">

<div class="title2">
<h2><span class="line"></span><span class="text">PopularProducts</span></h2>
</div>

<div class="clearfix margin_top7"></div>

<div class="one_fourth">

<a href="/products/coreldraw-graphics-suite-2017.php"><div class="tbox">

<img src="images/coreldraw-2017.png" alt="">

<h6>Coreldraw Graphics Suit 2017</h6>

<p>CorelDRAW® Graphics Suite 2017 is our latest and most innovative graphic design program yet! Get all of our industry-acclaimed tools found in CorelDRAW Graphics Suite X8, plus so much more.</p>


</div></a>

</div>


<div class="one_fourth">

<a href="/products/autodesk-autocad-2018.php"><div class="tbox">

<img src="images/AutoCAD-2018.jpg" alt="">

<h6>Autodesk AUTOCAD 2018</h6>

<p>Design every detail with Autodesk AutoCAD software, one of the world's leading CAD applications. Create stunning 2D and 3D designs with innovative tools that are always up-to-date.</p>


</div></a>

</div>



<div class="one_fourth">

<a href="/products/adobe-creative-cloud.php"><div class="tbox">

<img src="images/adobe-creative-cloud.jpg" alt="">

<h6>Adobe creative cloud CC 2017</h6>

<p>The world-class design tools in Creative Cloud give you everything you need to make anything you can dream up. Design logos, posters, brochures, ads, and more. Combine images to make incredible artwork.</p>


</div></a>

</div>

<div class="one_fourth last">

<a href="/products/adobe-photoshop-cc.php"><div class="tbox">

<img src="images/adobephotoshop.jpg" alt="">

<h6>Adobe Photoshop CC 2017</h6>


<p>From posters to packaging, basic banners to beautiful websites, unforgettable logos to eye-catching icons, Photoshop keeps the design world moving. With intuitive tools and easy-to-use templates.</p>


</div></a>

</div>



</div>
</div>
<div class="clearfix"></div>

<div class="features_sec30">



<div class="container">

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Key <strong> Highlights</strong><span></span></span></h2>

</div>

<div class="clearfix margin_top7"></div>

<div class="one_third">

<div class="left"><span aria-hidden="true" class="icon-cursor"></span></div>

<div class="right">
<h5 class="light">Licensed Software</h5>
<p>We help you comply major software by providing
100% authentic licensed softwares.</p>
</div>

<div class="clearfix margin_top7"></div>


</div>

<div class="one_third">

<div class="left"><span aria-hidden="true" class="icon-badge"></span></div>

<div class="right">
<h5 class="light">Best Pricing</h5>
<p>We offer affordable pricing and payment options for
major of our software products.</p>
</div>

<div class="clearfix margin_top7"></div>


</div>

<div class="one_third last">

<div class="left"><span aria-hidden="true" class="icon-settings"></span></div>

<div class="right">
<h5 class="light">Great Customer Support</h5>
<p>We offer great software support right from
Installations to trainings and troubleshooting.</p>
</div>

<div class="clearfix margin_top7"></div>



</div>

</div>
</div>

<div class="clearfix"></div>

<div class="features_sec31">
<div class="container">

<div class="counters1 two">

<div class="one_fourth"> <span id="target4">7</span> <h4>Years</h4> </div>

<div class="one_fourth"> <span id="target">1500</span> <h4>Clients</h4> </div>

<div class="one_fourth"> <span id="target2">5000</span> <h4>License Sold</h4></div>



<div class="one_fourth last"> <span id="target5">5</span> <h4>Awards</h4> </div>

</div>

</div>
</div>

<div class="clearfix"></div>


<div class="features_sec38">

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Testimonials<span></span></span></h2>

</div>

<div class="container">

<div class="flexslider carousel">
<ul class="slides">

<li>



<p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>

<h6>Austin Lisandro <em>- websitename -</em></h6>

</li><!-- end slide -->

<li>



<p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>


<h6>Alana Kasandra <em>- websitename -</em></h6>

</li><!-- end slide -->

<li>



<p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>


<h6>Jorge Anthony <em>- websitename -</em></h6>

</li><!-- end slide -->

<li>


<p><strong>Established fact that a reader will be distracted by the readable content of a page when looking at its layout.</strong> The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters their default model text andthere for an search for lorem ipsum will uncover many web sites still in their infancy various versions thereforealways have evolved over the years.</p>


<h6>Fernanda Elyse <em>- websitename -</em></h6>


</li><!-- end slide -->

</ul>
</div>

</div>
</div><!-- end features section 38 -->

<div class="clearfix"></div>

<div class="features_sec14">
<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Dealers <strong> Partners</strong><span></span></span></h2>

</div>

<div class="clearfix margin_top5"></div>
<div class="container">

  <div class="one_fourth">
<img src="images/corel300x80.png" alt="">
</div>

<div class="one_fourth">
<img src="images/adobe300x80.png" alt="">
</div>


<div class="one_fourth">
<img src="images/autodesk300x80.png" alt="">
</div>

<div class="one_fourth last">

<img src="images/microsoft300x80.png" alt="">
</div>

</div>
<div class="clearfix margin_top3"></div>
<div class="container">

<div class="one_fourth">
<img src="images/windows300x80.png" alt="">
</div>

<div class="one_fourth">
<img src="images/tally300x80.png" alt="">
</div>

<div class="one_fourth">
<img src="images/vray300x80.png" alt="">
</div>

<div class="one_fourth last">
<img src="images/quickheal300x80.png" alt="">
</div>

</div>
</div>

<div class="clearfix"></div>


<div class="parallax_section4">
<div class="container">

<h2>Happy to help you, always.</h2>

<p>Call or Email us to contact.</p>

<a href="/contact.php" class="button transp2">Request Quote</a>

</div>
</div>

<?php include 'includes/footer.php' ?>

<a href="#" class="scrollup">Scroll</a>

</div>

</div>




<script type="text/javascript" src="js/universal/jquery.js"></script>


<script src="js/animations/js/animations.min.js" type="text/javascript"></script>



<script src="js/mainmenu/bootstrap.min.js"></script> 
<script src="js/mainmenu/customeUI.js"></script> 


<script src="js/masterslider/jquery.easing.min.js"></script>
<script src="js/masterslider/masterslider.min.js"></script>

<script type="text/javascript">
(function($) {
"use strict";

var slider = new MasterSlider();
// adds Arrows navigation control to the slider.
slider.control('arrows');
slider.control('bullets');

slider.setup('masterslider' , {
width:1400,    // slider standard width
height:600,   // slider standard height
space:0,
speed:45,
layout:'fullwidth',
loop:true,
preload:0,
autoplay:true,
view:"basic"
});

})(jQuery);
</script>



<script src="js/scrolltotop/totop.js" type="text/javascript"></script>


<script type="text/javascript" src="js/mainmenu/sticky-main.js"></script>
<script type="text/javascript" src="js/mainmenu/modernizr.custom.75180.js"></script>



<script type="text/javascript" src="js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="js/cubeportfolio/main.js"></script>
<script type="text/javascript" src="js/cubeportfolio/main2.js"></script>



<!-- flexslider -->
<script defer src="js/flexslider/jquery.flexslider.js"></script>
<script defer src="js/flexslider/custom.js"></script>


<script type="text/javascript" src="js/universal/custom.js"></script>

<script src="js/aninum/jquery.animateNumber.min.js"></script>



<?php include 'includes/menujs.php' ?>
</body>
</html>
