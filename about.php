﻿<?php
include 'currenturl.php';

?>
<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
<title>About - Setutech</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" type="image/png" href="images/fav-2.png"/>

<!-- this styles only adds some repairs on idevices  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Google fonts - witch you want to use - (rest you can just remove) -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>


<!-- ######### CSS STYLES ######### -->

<link rel="stylesheet" href="css/reset.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />

<link rel="stylesheet" href="css/w3.css" type="text/css" />

<!-- font awesome icons -->
<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">

<!-- simple line icons -->
<link rel="stylesheet" type="text/css" href="css/simpleline-icons/simple-line-icons.css" media="screen" />



<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
<link rel="stylesheet" media="screen" href="css/shortcodes.css" type="text/css" /> 


<link href="js/mainmenu/bootstrap.min.css" rel="stylesheet">
<link href="js/mainmenu/menu-2.css" rel="stylesheet">



<link rel="stylesheet" type="text/css" href="js/tabs/assets/css/responsive-tabs3.css">

</head>

<body>

<div class="site_wrapper">

<?php include 'includes/header.php' ?>



<div class="clearfix"></div>

<div class="page_title4">
<div class="container">


<h3>Established in 2010, Setu Technologies, growing in all aspects with hard work and expertise. In last four years, we have become one of the best value added partners for Corel in Mumbai region with more than 1500 Customers. </h3>   

<div class="one_half">

<h6>We at Setu Technologies have customer satisfaction as our only inspiration. An official Corel Value Added Partner, we aim at delivering you the very best, always, officially! We deal in all legal software like Corel, Adobe, Microsoft, Quick Heal, Antivirus, Abby finereader, Firewall, Backup and many more.</h6>    
<h6>Our aim is to become best value added partner for all major Software vendors and provide cost effective and right solutions bundle with our prompt service to our customers who contributes to their business in a time, quality and cost effective manner. Scaling new horizons with broader perspectives, to satisfy you!
</h6>   

<h6>Setu Technologies help our customers drive enterprise-wide productivity, offer differentiated user experiences and open new growth opportunities. At Setu Technologies, our highly-experienced leadership team is committed to helping businesses invent the future with software innovation.</h6>  


</div>

<div class="one_half last">
<img src="/images/shutterstock_143568193.jpg" alt="">

</div>



</div>


</div><!-- end page title -->

<div class="features_sec32">
<div class="container">

<div class="title2">
<h2><span class="line"></span><span class="text">Features</span></h2>
</div>

<div class="clearfix margin_top3"></div>

<div class="one_third">

<div class="box">

<span aria-hidden="true" class="icon-screen-desktop"></span>
<br><br>
<h5>Licensed Software</h5>
<p>We help you comply major software by providing 100% authentic licensed softwares..</p>

</div><!-- end section -->

</div><!-- end all sections -->

<div class="one_third">

<div class="box">

<span aria-hidden="true" class="fa fa-money"></span>
<br><br>
<h5>Affordable Pricing</h5>
<p> We offer affordable pricing and payment options for major of our software products.</p>

</div><!-- end section -->

</div><!-- end all sections -->

<div class="one_third last">

<div class="box">

<span aria-hidden="true" class="fa fa-headphones"></span>
<br><br>
<h5>Great Software Support</h5>
<p>We offer great software support right from Installations to trainings and troubleshooting.</p>

</div><!-- end section -->

</div><!-- end all sections -->

</div>
</div>

<div class="features_sec31">
<div class="container">

<div class="counters1 two">

<div class="one_fourth"> <span id="target4">7 +</span> <h4>Years</h4> </div>

<div class="one_fourth"> <span id="target">1000 +</span> <h4>Clients</h4> </div>

<div class="one_fourth"> <span id="target2">5000 +</span> <h4>License Sold</h4> </div>

<div class="one_fourth last"> <span id="target5">5</span> <h4>Awards</h4> </div>

</div><!-- end counters1 section -->

</div>
</div>
<div class="clearfix"></div>


<div class="clearfix"></div>
<div class="features_sec14">
<div class="container">

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Official   <strong></strong> Partners</span></h2>

</div>
<div class="clearfix margin_top5"></div>
<div class="one_fifth animate fadeIn" data-anim-type="fadeIn" data-anim-delay="100">
<img src="/images/Adobe-logo-DW24-180px.jpg" alt="">
</div>

<div class="one_fifth animate fadeIn" data-anim-type="fadeIn" data-anim-delay="150">
<img src="/images/autodesk_partner_4.png" alt="">
</div>

<div class="one_fifth animate fadeIn" data-anim-type="fadeIn" data-anim-delay="200">
<img src="/images/logocorel80.jpg" alt="">
</div>

<div class="one_fifth animate fadeIn" data-anim-type="fadeIn" data-anim-delay="250">
<img src="/images/download.png" alt="">
</div>

<div class="one_fifth last animate fadeIn" data-anim-type="fadeIn" data-anim-delay="300">
<img src="/images/tally-.jpg" alt="">
</div>

</div>
</div>
<div class="clearfix"></div>

<div class="parallax_section4">
<div class="container">

<h2>Happy to help you, always.</h2>

<p>Call or Email us to contact.</p>

<a href="/contact.php" class="button transp2">Request Quote</a>

</div>
</div>



<?php include 'includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->





</div>


<script src="js/scrolltotop/totop.js" type="text/javascript"></script>


<?php include 'includes/menujs.php' ?>


</body>
</html>


