﻿<?php
include 'currenturl.php';

?>
<!doctype html>
<html lang="en-gb" class="no-js"> 

<head>
<title>Contact US</title>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" type="image/png" href="images/fav-2.png"/>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>


<link rel="stylesheet" href="css/reset.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />

<link rel="stylesheet" href="css/w3.css" type="text/css" />


<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">


<link rel="stylesheet" type="text/css" href="css/simpleline-icons/simple-line-icons.css" media="screen" />


<link href="js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />


<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />


<link rel="stylesheet" media="screen" href="css/shortcodes.css" type="text/css" /> 




<link href="js/mainmenu/bootstrap.min.css" rel="stylesheet">
<link href="js/mainmenu/menu-2.css" rel="stylesheet">


<link rel="stylesheet" href="js/form/sky-forms2.css" type="text/css" media="all">

</head>

<body>

<div class="site_wrapper">

<?php include 'includes/header.php' ?>

<div class="clearfix"></div>



<div class="content_fullwidth lessmore">


<div class="container">
<div class="clearfix marb3"></div>
<div class="two_third">
<div class="clearfix marb10"></div>
<p>Feel free to talk to our online representative at 10am - 6pm in time you please using our Live Chat system on our website or one of the below instant messaging programs.</p>
<br />
<header><strong>Call General Inquiries: +91 9004071773</strong></header>
<br /><br /><br />

<div class="cforms">

<form class="sky-form">
<header>Send Us a <strong>Message!</strong></header>
<fieldset>
  <div class="row">
    <section class="col col-6">
      <label class="label">Contact Name</label>
      <label class="input"> <i class="icon-append icon-user"></i>
        <input type="text" name="name" id="name">
      </label>
    </section>
    <section class="col col-6">
      <label class="label">Contact E-mail</label>
      <label class="input"> <i class="icon-append icon-envelope-alt"></i>
        <input type="email" name="email" id="email">
      </label>
    </section>
  </div>
  <div class="row">
    <section class="col col-6">
      <label class="label">Mobile Number</label>
      <label class="input"> <i class="icon-append icon-phone"></i>
        <input type="text" name="mobile" id="mobile">
      </label>
    </section>
    <section class="col col-6">
      <label class="label">Product Name</label>
      <label class="input"> <i class="icon-append icon-envelope-alt"></i>
        <input type="text" name="productName" id="productName">
      </label>
    </section>
  </div>
  <section>
    <label class="label">Message</label>
    <label class="textarea"> <i class="icon-append icon-comment"></i>
      <textarea rows="4" name="message" id="message"></textarea>
    </label>
  </section>

</fieldset>
 <p id="showError" class="showerror"></p>
<footer>
   <button type="button" id="submit" class="button">Request Quote</button>
</footer>

</form>

</div>

</div>

<div class="one_third last">
<div class="clearfix marb10"></div>
<div class="address_info two">

<h4 class="light">Company Address</h4>   <br />
<ul>
  <li> <strong>Name:</strong> Setu Technologies<br />   <br />
    <strong>Address:</strong> Shop No. 30, Shakti Nagar,C.S.C. Road No. 4,Dahisar (East),Mumbai, Maharashtra 400068
    <br />   <br />
    <strong>Mobile:</strong> +91 9004071773<br />   <br />
    <strong>E-mail:</strong> <a href="mailto:bhupesh@setutech.com">bhupesh@setutech.com</a><br />   <br />
    <strong>Website:</strong> <a href="index.html">www.setutech.com</a> </li>
</ul>

</div>



</div>

</div>


</div>
</div>



<?php include 'includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a>

</div>




<script src="js/scrolltotop/totop.js" type="text/javascript"></script>


<?php include 'includes/menujs.php' ?>


<script>
  
  $(document).ready(function(){

  var submit = $("#submit");

    var showError = $("#showError");

submit.click(function(){

var name = $('#name').val();
var mobile = $('#mobile').val();
var email = $('#email').val();
var productName = $('#productName').val();
var message = $('#message').val();


var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var add = /^[a-zA-Z0-9-\/] ?([a-zA-Z0-9-\/]|[a-zA-Z0-9-\/] )*[a-zA-Z0-9-\/]$/;


if (name == '') {
  showError.text("Please Enter Contact Name *");
} else if(!(re.test(email))){
  showError.text("Please Enter valid email id *");
} else if(mobile.length < 10 || mobile.length > 10 || mobile == ''){
  showError.text("Please Enter valid mobile number *");
}else if (productName == ''){
 showError.text("Please Enter Product Name *");
}else{

$.post("/inquiry.php",

{
name: name,
mobile: mobile,
email: email,
message: message,
productName: productName
},
function(data){

          submit.attr("disabled", "disabled");
          submit.css("color", "yellow");
          submit.text("Please wait...");
                    
          if (data == 0) {
            submit.text("Cannot Add Details");
            setTimeout(function() {
              function redirect() {
                window.location = "/index.php";
              }
              redirect();
            }, 3000);
          } else if (data == 1) {
                        setTimeout(function() {
              submit.css("color", "green");
              submit.text("Register Successfully");
            }, 2000);
            setTimeout(function() {
              function redirect() {
                window.location = "/thank-you.php";
              }
              redirect();
            }, 6000);
                                    
          } else {alert("js error");}


});

}




});



});
</script>



</body>
</html>


