﻿<?php
include 'currenturl.php';

?>
<!doctype html>
 <html lang="en-gb" class="no-js"> 

<head>
	<title>Services - Setutech</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    <link rel="shortcut icon" type="image/png" href="images/fav-2.png"/>
     
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
    

    
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/w3.css" type="text/css" />
    
    
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
	
    
	<link rel="stylesheet" type="text/css" href="css/simpleline-icons/simple-line-icons.css" media="screen" />
    
    
    <link href="js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
    
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    
    
    <link rel="stylesheet" media="screen" href="css/shortcodes.css" type="text/css" /> 
    

    
    
    <link href="js/mainmenu/bootstrap.min.css" rel="stylesheet">
     <link href="js/mainmenu/menu-2.css" rel="stylesheet">
    
   
</head>

<body>

<div class="site_wrapper">

<?php include 'includes/header.php' ?>



<div class="clearfix"></div>

<div class="page_title4">
<div class="container">

    
    <h3>Setutech Services:-</h3>   
<div class="clearfix margin_top10"></div>
    <div class="one_half">
        <div class="stcode_title8">
    
        <h2><span class="line"></span><span class="text">Lead 
 <strong>Generation</strong></span></h2>
    
    </div>
   <h6>Setu Technologies is a premier end-to-end lead generation provider. We are effectively an extension of your business, geared primarily to find innovative ways to maximize sales opportunities and manage client relationships. We do this by crafting well tailored strategies around data lists, telemarketing and appointment setting.</h6>  
    
    </div>

    <div class="one_half last">
         <img src="/images/Lead-Generation.jpg" alt="">
        
    </div>

    <div class="clearfix margin_top10"></div>

     <div class="one_half">

          <img src="/images/training.jpg" alt="">
        
    
    </div>

    <div class="one_half last">
       <div class="stcode_title8">
    
        <h2><span class="line"></span><span class="text">Corporate  <strong>Training</strong></span></h2>
    
    </div>
         <h6>Our flexible Corporate Training programs aim at your competency development be it project specific or general ranging from technology to soft skills. To keep ahead of the competition, organizations and businesses need to invest heavily in getting workforce trained. Setu Technologies adopt an innovative approach to ensure an in-depth understanding of customer’s business needs and then provide them, with Best-Fit Approach.</h6>  
    </div>
<div class="clearfix margin_top10"></div>
     <div class="one_half">
        <div class="stcode_title8">
    
        <h2><span class="line"></span><span class="text">Audit  <strong>and Consultation</strong></span></h2>
    
    </div>
   <h6>A Software Asset Management audit enables you to maintain continuous software license compliance and ensures you will be prepared for any vendor software audits. A failed audit can result in potential penalties and UN-budgeted license expenses. This happens predominantly when IT managers lack visibility into actual software usage, frequently over or underestimating their software needs in an effort to balance cost and end user productivity.</h6>  
    
    </div>

    <div class="one_half last">
         <img src="/images/consultation.jpg" alt="">
        
    </div>

<div class="clearfix margin_top10"></div>
     <div class="one_half">
        
    <img src="/images/Installation-services.jpg" alt="">
    
    </div>

    <div class="one_half last">
        <div class="stcode_title8">
    
        <h2><span class="line"></span><span class="text">Installation  <strong>Services</strong></span></h2>
    
    </div>
         <h6>We are committed in providing the highest quality of service and support to our valued customers so that you can make the most of your software experience. Get in touch by chat, email or phone for product support, to purchase a product, or find out more about your order.</h6>  
        
    </div>
 
</div>



</div>

<div class="parallax_section4">
<div class="container">
    
    <h2>Happy to help you, always.</h2>
    
    <p>Call or Email us to contact.</p>
    
    <a href="/contact.php" class="button transp2">Request Quote</a>

</div>
</div>

<?php include 'includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a>





</div>


<script src="js/scrolltotop/totop.js" type="text/javascript"></script>



<?php include 'includes/menujs.php' ?>


</body>
</html>


