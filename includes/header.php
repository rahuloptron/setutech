<header class="header">
 
    <div class="container">
    
    <!-- Logo -->
    <div class="logo"><a href="/index.php" id="logo2"></a></div>
        
    <!-- Navigation Menu -->
    <nav class="menu_main">
        
   
      <div class="w3-sidebar w3-bar-block w3-card w3-animate-right scroll " style="display:none;right:0;" id="rightMenu">
  <button onclick="closeRightMenu()" class="w3-bar-item w3-button w3-large">&times;</button>
 
              
             <a href="/index.php" 
             <?php 

             if($currentURL == "/index.php")

              { echo "class='w3-text-indigo'"; } 

            else{

              echo "class='w3-bar-item w3-button'";

              }

            ?>

              >Home</a>

              <a href="/about.php" <?php if($currentURL == "/about.php"){ echo "class='w3-text-indigo'"; } 

              else{

              echo "class='w3-bar-item w3-button'";

                }


              ?> >About Us</a>  

                <button class="w3-button w3-block w3-left-align" onclick="menu()">
                Products<i class="fa fa-caret-down"></i>
                </button>


                <div id="menu" class="w3-hide w3-grey w3-card">
                

                    <button class="w3-button w3-block w3-left-align-submenu"  onclick="submenu()">
                        Corel<i class="fa fa-caret-down"></i>
                        </button>


                         <div id="submenu" class="w3-hide w3-white w3-card">
                        

                           
                                 <a href="/products/coreldraw-graphics-suite-2017.php"  <?php if($currentURL == "/products/coreldraw-graphics-suite-2017.php"){ echo "class='w3-text-indigo'"; } 

                                  else{

                                  echo "class='w3-bar-item w3-button'";

                                    }


                                  ?> >Coreldraw graphics suite 2017</a>  

                                              
                               
                          </div>



                    <button class="w3-button w3-block w3-left-align-submenu" onclick="submenu2()">
                        Autodesk<i class="fa fa-caret-down"></i>
                    </button>


                           <div id="submenu2" class="w3-hide w3-white w3-card">
                        

                        
                                 <a href="/products/autodesk-autocad-2018.php"  <?php if($currentURL == "/products/autodesk-autocad-2018.php"){ echo "class='w3-text-indigo'"; } 

                                  else{

                                  echo "class='w3-bar-item w3-button'";

                                    }


                                  ?> >Autodesk Autocad 2018</a>  

                    
                          </div>

                             <button class="w3-button w3-block w3-left-align-submenu" onclick="submenu3()">
                        Adobe<i class="fa fa-caret-down"></i>
                    </button>


                           <div id="submenu3" class="w3-hide w3-white w3-card">
                        

                        
                                 <a href="/products/adobe-photoshop-cc.php"  <?php if($currentURL == "/products/adobe-photoshop-cc.php"){ echo "class='w3-text-indigo'"; } 

                                  else{

                                  echo "class='w3-bar-item w3-button'";

                                    }


                                  ?> >Photoshop CC 2018</a>  

                                  <a href="/products/adobe-illustrator-cc.php"  <?php if($currentURL == "/products/adobe-illustrator-cc.php"){ echo "class='w3-text-indigo'"; } 

                                  else{

                                  echo "class='w3-bar-item w3-button'";

                                    }


                                  ?> >Illustrator CC</a>  

                                  <a href="/products/adobe-creative-cloud.php"  <?php if($currentURL == "/products/adobe-creative-cloud.php"){ echo "class='w3-text-indigo'"; } 

                                  else{

                                  echo "class='w3-bar-item w3-button'";

                                    }


                                  ?> >Creative Cloud for Teams</a>  

                                  <a href=""  <?php if($currentURL == ""){ echo "class='w3-text-indigo'"; } 

                                  else{

                                  echo "class='w3-bar-item w3-button'";

                                    }


                                  ?> >Acrobat pro DC</a>  

                    
                          </div>
                        
                </div>



             <a href="#"  <?php if($currentURL == "#"){ echo "class='w3-text-indigo'"; } 

              else{

              echo "class='w3-bar-item w3-button'";

                }


              ?> >Plugins</a>  
            

             <a href="/services.php"  <?php if($currentURL == "/services.php"){ echo "class='w3-text-indigo'"; } 

              else{

              echo "class='w3-bar-item w3-button'";

                }


              ?> >Services</a>  
        
            

             <a href="/contact.php" <?php if($currentURL == "/contact.php"){ echo "class='w3-text-indigo'"; } 

              else{

              echo "class='w3-bar-item w3-button'";

                }


              ?> >Contact US</a>  
            
        

</div>

<div class="w3-teal">

  <button class="w3-button w3-teal w3-xlarge w3-right" onclick="openRightMenu()"><i class="fa fa-bars" aria-hidden="true"></i>

</button>
 
</div>

    </nav><!-- end Navigation Menu -->
    
   
    
</header>