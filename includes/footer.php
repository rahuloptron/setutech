<footer class="footer style2">


<div class="copyright_info two">
<div class="container">

	<div class="clearfix divider_dashed10"></div>
    
    <div class="one_half" data-anim-type="fadeInRight">
    
        Copyright © 2017 Setutech.com All rights reserved.  <a href="#">Terms of Use</a> | <a href="#">Privacy Policy</a>
        
    </div>
    
    
    
</div>
</div><!-- end copyright info -->


</footer>
