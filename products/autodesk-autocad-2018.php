
<?php
include '../currenturl.php';

?>
<!doctype html>
 <html lang="en-gb" class="no-js"> 

<head>
<title>Autodesk Autocad 2018</title>

<?php

$product = 'Autodesk Autocad 2018'

?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" type="image/png" href="../images/fav-2.png"/>


<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>


<link rel="stylesheet" href="../css/reset.css" type="text/css" />
<link rel="stylesheet" href="../css/style.css" type="text/css" />
  <link rel="stylesheet" href="../css/w3.css" type="text/css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">


<link rel="stylesheet" type="text/css" href="../css/simpleline-icons/simple-line-icons.css" media="screen" />


<link href="../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />


<link rel="stylesheet" media="screen" href="../css/responsive-leyouts.css" type="text/css" />


<link rel="stylesheet" media="screen" href="../css/shortcodes.css" type="text/css" /> 


<link href="../js/mainmenu/bootstrap.min.css" rel="stylesheet">
 <link href="../js/mainmenu/menu-2.css" rel="stylesheet">


<link href="../js/carouselowl/owl.transitions.css" rel="stylesheet">
<link href="../js/carouselowl/owl.carousel.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../js/form/sky-forms3.css">

</head>

<body>

<div class="site_wrapper">

<?php include '../includes/header.php' ?>


<div class="content_fullwidth">

<div class="features_sec8">
<div class="container">

    <div class="flexslider carousel">
        
    
               <div class="left">
                    <img src="../images/big-autocad-2018.jpg" alt="" draggable="false">
                
                </div>
                
                <div class="right">


                    <h1><strong>Autodesk </strong> Autocad 2018</h1>
                    <span></span>
                    <br><br>
                    <p>Design every detail with Autodesk AutoCAD software, one of the world's leading CAD applications. Create stunning 2D and 3D designs with innovative tools that are always up-to-date–and can produce almost any shape imaginable - with your personalized design solution.</p>
                    <br><br>
                   
               <a href="#bottom" id="click" class="but_phone">Request Quote</a>
                    
                
                </div>
            
          
            
          </div></div>

</div>

<div class="clearfix margin_top3"></div>


<div class="features_sec3">
<div class="container">

    <div class="title2">
        <h2><span class="line"></span><span class="text">Benefits</span></h2>
    </div>
    
    <div class="clearfix margin_top6"></div>
    
    <div class="one_third">
    
        <div class="box animate fadeIn" data-anim-type="fadeIn" data-anim-delay="100">
        
            <div class="ciref outline-outward left"></div>
            
            <div class="right">
                <h5>Create stunning designs</h5>
                <p>Design and visualize virtually any concept with 3D free-form tools.</p>
            </div>
        
        </div><!-- end section -->
        
        <div class="box animate fadeIn" data-anim-type="fadeIn" data-anim-delay="100">
        
            <div class="ciref outline-outward left"> </div>
            
            <div class="right">
                <h5>Document precisely</h5>
                <p>Speed documentation by automating common tasks and streamlining workflows.</p>
            </div>
        
        </div><!-- end section -->
        
        
    </div><!-- end all sections -->
    
    <div class="one_third">
    
        <div class="box animate fadeIn" data-anim-type="fadeIn" data-anim-delay="150">
        
            <div class="ciref outline-outward left active">  </div>
                <div class="right">
                <h5>Connect more quickly</h5>
                <p>Engage with colleagues using connected design technologies.</p>
            </div>
             
            
            
        
        </div><!-- end section -->
        
        <div class="box animate fadeIn" data-anim-type="fadeIn" data-anim-delay="150">
        
            <div class="ciref outline-outward left"> </div>
            <div class="right">
                <h5>Integrate your designs</h5>
                <p>AutoCAD presents new and enhanced features specifically for the construction industry, from building construction to industrial construction.</p>
            </div>
           
        
        </div><!-- end section -->
        
       
        
    </div><!-- end all sections -->
    
    <div class="one_third last">
    
        <div class="box animate fadeIn" data-anim-type="fadeIn" data-anim-delay="200">
        
            <div class="ciref outline-outward left">  </div>
            <div class="right">
                <h5>Customize your software</h5>
                <p>Configure AutoCAD software in ways you never thought possible.</p>
            </div>
           
        
        </div><!-- end section -->
        
        <div class="box animate fadeIn" data-anim-type="fadeIn" data-anim-delay="200">
        
            <div class="ciref outline-outward left"> </div>
            
            <div class="right">
                <h5>Take your drawings everywhere</h5>
                <p>Take your AutoCAD drawings everywhere with AutoCAD 360. This free, easy-to-use mobile viewing and markup application.</p>
            </div> 
        
        </div><!-- end section -->
        
        
    </div><!-- end all sections -->

</div>
</div>

<div class="clearfix margin_top3"></div>

<div class="features_sec53">
<div class="container">

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Features</span></h2>

</div>

<div class="clearfix margin_top5"></div>

<div class="one_half">
<div class="box">


<ul class="list1"> 



<li><i class="fa fa-caret-right"></i> Resizable dialog boxes - View more information with less scrolling.</li>
<li><i class="fa fa-caret-right"></i> Enhanced PDFs
 - PDFs are smaller, smarter, and more searchable.
</li>
<li><i class="fa fa-caret-right"></i> Smart dimensioning - Create measurements based on your drawing context.
</li>
<li><i class="fa fa-caret-right"></i> Revision clouds
 - Create and modify clouds more easily.
</li>
<li><i class="fa fa-caret-right"></i> Refined interface
 - THe darker interface helps reduce eyestrain.
</li>
<li><i class="fa fa-caret-right"></i> Stunning Visual Experience
 - See the details in your drawings more clearly.
</li>
<li><i class="fa fa-caret-right"></i> Reality computing
 - Orient your work more precisely with the point cloud.
</li>
<li><i class="fa fa-caret-right"></i> Inventor file import
 - Import Inventor models.
</li>



</ul>

</div>
</div>



<div class="one_half last">
<div class="box">

<ul class="list1">       
<li><i class="fa fa-caret-right"></i> Online maps
 - Capture online maps as static images and print them.
</li>
<li><i class="fa fa-caret-right"></i> MSurface Curve Extraction tool
 - Extract isoline curves.

</li>
<li><i class="fa fa-caret-right"></i> 3D free-form design tools - Generate design ideas in almost any form.</li>
<li><i class="fa fa-caret-right"></i> Surface analysis - Analyze the continuity between different surfaces.
</li>
<li><i class="fa fa-caret-right"></i> In-canvas viewport controls - Change viewport settings, views, visual styles.
</li>

<li><i class="fa fa-caret-right"></i> Design feed - Work on an intranet, Internet, or cloud connection.

</li>

<li><i class="fa fa-caret-right"></i> Connected design solutions- Integrate your desktop, cloud, and mobile workflow.
</li>
<li><i class="fa fa-caret-right"></i>Social media sharing - Use built-in connections for Fb, Twitter.</li>



</ul>

</div>
</div>



</div>

</div>
<div class="clearfix"></div>


<div class="features_sec4">
<div class="container">

    <div class="onecol_sixty">
    
        <h3 class="unline"> System Requirments</h3>
        
<ul class="list1">       
<li><i class="fa fa-caret-right"></i> Windows 10, 8.1 or Windows 7, in 32-bit or 64-bit, all with latest Updates and Service Pack</li>
<li><i class="fa fa-caret-right"></i> Intel Core i3/5/7 or AMD Athlon 64            </li>
<li><i class="fa fa-caret-right"></i> 2 GB RAM          </li>
<li><i class="fa fa-caret-right"></i> 1 GB hard disk space
</li>
<li><i class="fa fa-caret-right"></i> Multi-touch screen, mouse or tablet
</li>
<li><i class="fa fa-caret-right"></i> 1280 x 720 screen resolution at 100% (96 dpi)
</li>
<li><i class="fa fa-caret-right"></i> Microsoft Internet Explorer 11 or higher
</li>
<li><i class="fa fa-caret-right"></i> Microsoft .Net Framework 4.6
</li>
<li><i class="fa fa-caret-right"></i> DVD drive optional (for box installation)
</li>


</ul>
  
    </div>

    <div class="onecol_forty last">
    
        <div class="peosays">
        
            <h3 class="unline"> What People Says</h3>
            
            <div class="clearfix"></div>
            
            <div id="owl-demo11" class="owl-carousel small four">
                
                <div class="box">
                
                    <div><h6>Name <em>www.websitenames.com</em></h6></div>
                    
                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy generators on the versions have evolved over the years.</p>
                    
                    <span> Rating: &nbsp; <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>
                    
                </div>
                
                <div class="box">
                
                    <div><h6>Name 2 <em>www.websitenames.com</em></h6></div>
                    
                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy generators on the versions have evolved over the years.</p>
                    
                    <span> Rating: &nbsp; <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>
                    
                </div>
                
                
            </div>
        
        </div>

     </div>

</div>
</div>


<div class="clearfix"></div>

<div class="features_sec5">

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Support</span></h2>

</div>
<div class="clearfix margin_top6"></div>
<div class="container">
    
    <div class="onecol_sixty">
    
        
        <h5>Setu Technologies have customer satisfaction as our only inspiration. An official Value Added partner, we aim at delivering you the very best, always, officially!!! We are committed in providing the highest quality of service and support to our valued customers so that you can make the most of your software experience. Get in touch by chat, email or phone for product support.</h5>
        
        <ul class="list1">
            <li><i class="fa fa-caret-right"></i>Free Installation of the software at the time of purchase. Our person will come to your office and will do the installation OR Remote installations are also supported if required by clients. (Subjected to your windows is properly updated) </li>
             <li><i class="fa fa-caret-right"></i>We provide Online and Telephonic support during office Hours to all our clients. (Remote Access using AnyDesk & Ammyy Admin)</li>
              <li><i class="fa fa-caret-right"></i>We also involve Corel Technical Team for any 2nd level of Support if approved.</li>
               <li><i class="fa fa-caret-right"></i>We still provide basic technical support to all our existing customers even if the maintenance gets expired.</li>
                <li><i class="fa fa-caret-right"></i>We also help customers to Upgrade their products on timely basis once launched.</li>


        </ul>
       
    
    </div><!-- end section -->
    
    <div class="onecol_forty last animate fadeInRight" data-anim-type="fadeInRight" data-anim-delay="300">
    
        <img src="/images/setutech-support.jpg" alt="" class="rimg">
    
    </div><!-- end section -->
    
    <div class="clearfix margin_top7"></div>
    
   
</div>
</div>
<div class="clearfix"></div>

<div class="content_fullwidth less">


<div class="clearfix marb10"></div>

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Request a Quote</span></h2>

</div>
<div class="clearfix marb5"></div>

<div class="container">

      <div class="one_full">
   
        
        <div class="cforms">
        
        <form class="sky-form2">
         
          <fieldset>
            <div class="row">
              <section class="col col-6">
                <label class="label">Name</label>
                <label class="input"> <i class="icon-append icon-user"></i>
                  <input type="text" name="name" id="name">
                </label>
              </section>
              <section class="col col-6">
                <label class="label">E-mail</label>
                <label class="input"> <i class="icon-append icon-envelope-alt"></i>
                  <input type="email" name="email" id="email">
                </label>
              </section>
               <section class="col col-6">
                <label class="label">Mobile</label>
                <label class="input"> <i class="icon-append icon-phone"></i>
                  <input type="number" name="mobile" id="mobile">
                    <input type="hidden" value="<?php echo $product ?>" name="productName" id="productName">
                </label>
              </section>
            </div>
           
            <section>
              <label class="label">Message</label>
              <label class="textarea"> <i class="icon-append icon-comment"></i>
                <textarea rows="4" name="message" id="message"></textarea>
              </label>
            </section>
            
          </fieldset>
            <p id="showError" class="showerror"></p>
          <footer>
            <button type="button" id="submit" class="button">Request Quote</button>
          </footer>
          <div class="message"> <i class="icon-ok"></i>
            <p>Your message was successfully sent!</p>
          </div>
        </form>
        
        </div>
        
      </div><!-- end section -->
      


</div>
</div>

</div>

<div class="clearfix"></div>

<?php include '../includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a>


</div>


<script src="../inquiry.js"></script>


<script src="../js/scrolltotop/totop.js" type="text/javascript"></script>

<script src="../js/carouselowl/owl.carousel.js"></script>
<script src="../js/carouselowl/custom.js"></script>

<script>
$(document).ready(function(){
    $("#click").click(function(){
       $("html, body").animate({ scrollTop: $(document).height() }, "slow");
    });
});
</script>

<?php include '../includes/menujs.php' ?>


</body>
</html>


