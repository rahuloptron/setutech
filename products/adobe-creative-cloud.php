
<?php
include '../currenturl.php';

?>
<!doctype html>
 <html lang="en-gb" class="no-js"> 

<head>
<title>Adobe Creative Cloud</title>

<?php

$product = 'Adobe Creative Cloud'

?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" type="image/png" href="../images/fav-2.png"/>


<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>


<link rel="stylesheet" href="../css/reset.css" type="text/css" />
<link rel="stylesheet" href="../css/style.css" type="text/css" />
  <link rel="stylesheet" href="../css/w3.css" type="text/css" />

<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<link rel="stylesheet" type="text/css" href="../js/form/sky-forms3.css">


<!-- animations -->
<link href="../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />

<!-- responsive devices styles -->
<link rel="stylesheet" media="screen" href="../css/responsive-leyouts.css" type="text/css" />

<!-- shortcodes -->
<link rel="stylesheet" media="screen" href="../css/shortcodes.css" type="text/css" /> 


<!-- mega menu -->
<link href="../js/mainmenu/bootstrap.min.css" rel="stylesheet">
 <link href="../js/mainmenu/menu-2.css" rel="stylesheet">


<!-- owl carousel -->
<link href="../js/carouselowl/owl.transitions.css" rel="stylesheet">
<link href="../js/carouselowl/owl.carousel.css" rel="stylesheet">


</head>

<body>

<div class="site_wrapper">

<?php include '../includes/header.php' ?>


<div class="content_fullwidth">

<div class="features_sec8">
<div class="container">

    <div class="flexslider carousel">
        
	
                <div class="left">
                    <img src="../images/Adobe-Creative-Cloud-2017-Colectiion.jpg" alt="" draggable="false">
                
                </div>
                
                <div class="right">


                    <h1><strong>Adobe </strong> Creative Cloud CC 2017</h1>
                    <span></span>
                    <br><br>
                    <p>The world-class design tools in Creative Cloud give you everything you need to make anything you can dream up. Design logos, posters, brochures, ads, and more. Combine images to make incredible artwork. And use our mobile apps to sketch, draw, and create layouts wherever you’re inspired.</p>
                    <br><br>

                   
                <a href="#bottom" id="click" class="but_phone">Request Quote</a>
                    
                
                </div>
            
          
            
          </div></div>

</div>

<div class="clearfix margin_top3"></div>


<div class="features_sec37">
<div class="container">
	
    <div class="stcode_title11">
    
    	<h2>From blank page to brilliant design.
        <em>The world-class design tools in Creative Cloud give you everything you need to make anything you can dream up. Design logos, posters, brochures, ads, and more. Combine images to make incredible artwork. And use our mobile apps to sketch, draw, and create layouts wherever you’re inspired.</em>
        <span class="line"></span></h2>
    
    </div>
    
    <div class="clearfix margin_top4"></div>
    
    <ul class="pop-wrapper">
    
        <li> <img src="../images/ps.png" alt=""> <h6>Photoshop</h6> </li>
        
        <li> <img src="../images/Illustrator.png" alt=""> <h6> Illustrator</h6></li>
        
        <li><img src="../images/InDesign.png" alt=""> <h6> InDesign</h6></li>

         <li><img src="../images/AdobeStock.png" alt=""> <h6> Adobe Stock</h6> </li>

        <li><img src="../images/IllustratorDraw.png" alt=""> <h6> Illustrator Draw</h6> </li>

        <li><img src="../images/Typekit.png" alt=""> <h6> Typekit</h6> </li>
        
        
    </ul>

</div>
</div>

<div class="clearfix margin_top3"></div>

<div class="features_sec53">
<div class="container">

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Adobe Creative Cloud All Apps</span></h2>

</div>

<div class="clearfix margin_top5"></div>

<div class="one_half">
<div class="box">


<ul class="list1"> 



<li><i class="fa fa-caret-right"></i> Adobe Acrobat</li>
<li><i class="fa fa-caret-right"></i> Photoshop CC
</li>
<li><i class="fa fa-caret-right"></i> Illustrator CC
</li>
<li><i class="fa fa-caret-right"></i> Dreamweaver CC</li>
<li><i class="fa fa-caret-right"></i> Lightroom CC
</li>
<li><i class="fa fa-caret-right"></i> Indesign CC</li>
<li><i class="fa fa-caret-right"></i> Premier Pro CC</li>

<li><i class="fa fa-caret-right"></i>Media Encoder CC</li>
<li><i class="fa fa-caret-right"></i>SpeedGrade CC</li>
<li><i class="fa fa-caret-right"></i>Flash Builder Premium</li>
<li><i class="fa fa-caret-right"></i>Scout CC</li>

</ul>

</div>
</div>



<div class="one_half last">
<div class="box">

<ul class="list1">       
<li><i class="fa fa-caret-right"></i> After Effects CC
</li>
<li><i class="fa fa-caret-right"></i> InCopy CC
</li>
<li><i class="fa fa-caret-right"></i> Animate CC</li>
<li><i class="fa fa-caret-right"></i> Fuse CC
</li>
<li><i class="fa fa-caret-right"></i> Muse CC
</li>

<li><i class="fa fa-caret-right"></i> Bridge CC

</li>

<li><i class="fa fa-caret-right"></i> Prelude CC
</li>

<li><i class="fa fa-caret-right"></i>Gaming SDK</li>
<li><i class="fa fa-caret-right"></i>Extension Manager CC</li>
<li><i class="fa fa-caret-right"></i>Extendscript Toolkit CC</li>
<li><i class="fa fa-caret-right"></i>Touch App Plugins</li>

</ul>

</div>
</div>



</div>

</div>
<div class="clearfix"></div>


<div class="features_sec4">
<div class="container">

	<div class="onecol_sixty">
    
    	<h3 class="unline">Benefits Apps</h3>
    	
<ul class="list1">       
<li><i class="fa fa-caret-right"></i> File Sync All Apps - 100GB per user</li>
<li><i class="fa fa-caret-right"></i> Centralized IT deployment tools - Self-install options also available</li>
<li><i class="fa fa-caret-right"></i> User Owned - Adobe ID </li>
<li><i class="fa fa-caret-right"></i> All creative desktop apps, services and business features</li>
<li><i class="fa fa-caret-right"></i> Desktop and mobile apps</li>
<li><i class="fa fa-caret-right"></i> Adobe Typekit and Behance
</li>
<li><i class="fa fa-caret-right"></i> Ability to Reassign Licenses
</li>
<li><i class="fa fa-caret-right"></i> License management tools - Admin Console
</li>

</ul>
  
    </div>

    <div class="onecol_forty last">
    
		<div class="peosays">
        
            <h3 class="unline"> What People Says</h3>
            
            <div class="clearfix"></div>
            
            <div id="owl-demo11" class="owl-carousel small four">
                
            	<div class="box">
                
                	<div><h6>Name <em>www.websitenames.com</em></h6></div>
                    
                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy generators on the versions have evolved over the years.</p>
                    
                    <span> Rating: &nbsp; <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>
                    
                </div>
                
                <div class="box">
                
                	<div><h6>Name 2 <em>www.websitenames.com</em></h6></div>
                    
                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy generators on the versions have evolved over the years.</p>
                    
                    <span> Rating: &nbsp; <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>
                    
                </div>
                
                
            </div>
        
        </div>

     </div>

</div>
</div>


<div class="clearfix"></div>

<div class="features_sec5">

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Support</span></h2>

</div>
<div class="clearfix margin_top6"></div>
<div class="container">
    
    <div class="onecol_sixty">
    
        
        <h5>Setu Technologies have customer satisfaction as our only inspiration. An official Value Added partner, we aim at delivering you the very best, always, officially!!! We are committed in providing the highest quality of service and support to our valued customers so that you can make the most of your software experience. Get in touch by chat, email or phone for product support.</h5>
        
        <ul class="list1">
            <li><i class="fa fa-caret-right"></i>Free Installation of the software at the time of purchase. Our person will come to your office and will do the installation OR Remote installations are also supported if required by clients. (Subjected to your windows is properly updated) </li>
             <li><i class="fa fa-caret-right"></i>We provide Online and Telephonic support during office Hours to all our clients. (Remote Access using AnyDesk & Ammyy Admin)</li>
              <li><i class="fa fa-caret-right"></i>We also involve Corel Technical Team for any 2nd level of Support if approved.</li>
               <li><i class="fa fa-caret-right"></i>We still provide basic technical support to all our existing customers even if the maintenance gets expired.</li>
                <li><i class="fa fa-caret-right"></i>We also help customers to Upgrade their products on timely basis once launched.</li>


        </ul>
       
    
    </div><!-- end section -->
    
    <div class="onecol_forty last animate fadeInRight" data-anim-type="fadeInRight" data-anim-delay="300">
    
        <img src="/images/setutech-support.jpg" alt="" class="rimg">
    
    </div><!-- end section -->
    
    <div class="clearfix margin_top7"></div>
    
   
</div>
</div>
<div class="clearfix"></div>

<div class="content_fullwidth less">


<div class="clearfix marb10"></div>

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Request a Quote</span></h2>

</div>
<div class="clearfix marb5"></div>

<div class="container">

      <div class="one_full">
   
        
        <div class="cforms">
        
        <form class="sky-form2">
         
          <fieldset>
            <div class="row">
              <section class="col col-6">
                <label class="label">Name</label>
                <label class="input"> <i class="icon-append icon-user"></i>
                  <input type="text" name="name" id="name">
                </label>
              </section>
              <section class="col col-6">
                <label class="label">E-mail</label>
                <label class="input"> <i class="icon-append icon-envelope-alt"></i>
                  <input type="email" name="email" id="email">
                </label>
              </section>
               <section class="col col-6">
                <label class="label">Mobile</label>
                <label class="input"> <i class="icon-append icon-phone"></i>
                  <input type="number" name="mobile" id="mobile">
                    <input type="hidden" value="<?php echo $product ?>" name="productName" id="productName">
                </label>
              </section>
            </div>
           
            <section>
              <label class="label">Message</label>
              <label class="textarea"> <i class="icon-append icon-comment"></i>
                <textarea rows="4" name="message" id="message"></textarea>
              </label>
            </section>
            
          </fieldset>
             <p id="showError" class="showerror"></p>
          <footer>
            <button type="button" id="submit" class="button">Request Quote</button>
          </footer>
          <div class="message"> <i class="icon-ok"></i>
            <p>Your message was successfully sent!</p>
          </div>
        </form>
        
        </div>
        
      </div><!-- end section -->
      


</div>
</div>

</div>

<div class="clearfix"></div>

<?php include '../includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a>


</div>



<script src="../inquiry.js"></script>


<!-- scroll up -->
<script src="../js/scrolltotop/totop.js" type="text/javascript"></script>


<!-- owl carousel -->
<script src="../../js/carouselowl/owl.carousel.js"></script>
<script src="../../js/carouselowl/custom.js"></script>



<script>
$(document).ready(function(){
    $("#click").click(function(){
       $("html, body").animate({ scrollTop: $(document).height() }, "slow");
    });
});
</script>

<script type="text/javascript" src="../js/universal/custom.js"></script>


<?php include '../includes/menujs.php' ?>


</body>
</html>


