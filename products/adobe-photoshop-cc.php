
<?php
include '../currenturl.php';

?>
<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
<title>adobe photoshop cc</title>

<?php

$product = 'adobe photoshop cc'

?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="shortcut icon" type="image/png" href="../images/fav-2.png"/>

<!-- this styles only adds some repairs on idevices  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Google fonts - witch you want to use - (rest you can just remove) -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>


<link rel="stylesheet" href="../css/reset.css" type="text/css" />
<link rel="stylesheet" href="../css/style.css" type="text/css" />
  <link rel="stylesheet" href="../css/w3.css" type="text/css" />
<!-- font awesome icons -->
<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">

<!-- simple line icons -->
<link rel="stylesheet" type="text/css" href="../css/simpleline-icons/simple-line-icons.css" media="screen" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


<link rel="stylesheet" type="text/css" href="../js/form/sky-forms3.css">


<!-- animations -->
<link href="../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />

<!-- responsive devices styles -->
<link rel="stylesheet" media="screen" href="../css/responsive-leyouts.css" type="text/css" />

<!-- shortcodes -->
<link rel="stylesheet" media="screen" href="../css/shortcodes.css" type="text/css" /> 


<!-- mega menu -->
<link href="../js/mainmenu/bootstrap.min.css" rel="stylesheet">
 <link href="../js/mainmenu/menu-2.css" rel="stylesheet">


<!-- owl carousel -->
<link href="../js/carouselowl/owl.transitions.css" rel="stylesheet">
<link href="../js/carouselowl/owl.carousel.css" rel="stylesheet">


</head>

<body>

<div class="site_wrapper">

<?php include '../includes/header.php' ?>


<div class="content_fullwidth">

<div class="features_sec8">
<div class="container">

    <div class="flexslider carousel">
        
	
                <div class="left">
                    <img src="../images/photoshop-product.jpg" alt="" draggable="false">
                
                </div>
                
                <div class="right">

                   
                    <h1><strong>Adobe </strong> photoshop CC 2017</h1>
                    <span></span>
                    <br><br>
                    <p>From posters to packaging, basic banners to beautiful websites, unforgettable logos to eye-catching icons, Photoshop keeps the design world moving. With intuitive tools and easy-to-use templates, even beginners can make something amazing.</p>
                    <br><br>
                   
               <a href="#bottom" id="click" class="but_phone">Request Quote</a>
                    
                
                </div>
            
          
            
          </div></div>

</div>

<div class="clearfix margin_top3"></div>

<div class="features_sec59 two">
<div class="container">
    
    <div class="one_full stcode_title9">
    
        <h2>The creative world runs on Photoshop.
        <span class="line"></span></h2>

    </div>
    
    <div class="clearfix marb4"></div>
    
    <div id="owl-demo6" class="owl-carousel">
    
            <div>
            
                <div class="one_half"><img src="/images/riverflow-ps-720x520-1.jpg" alt="" /></div>
                
                <div class="one_half last">
                
                    <h3 class="color">Designed for anyone to design anything.</h3>
                    
                
                    
                    <p>From posters to packaging, basic banners to beautiful websites, unforgettable logos to eye-catching icons, Photoshop keeps the design world moving. With intuitive tools and easy-to-use templates, even beginners can make something amazing.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div><!-- end section -->
            
            <div>
            
                <div class="one_half"><img src="/images/riverflow-ps-720x520-2.jpg" alt="" /></div>
                
                <div class="one_half last">
                
                    <h3 class="color">Not just photo taking. Breathtaking.</h3>
                    
                    
                    <p>Whether you’re looking for everyday edits or total transformations, Photoshop offers a complete set of professional photography tools to turn your snapshots into works of art. Adjust, crop, remove objects, retouch, and repair old photos. Play with color, effects, and more to turn the ordinary into something extraordinary.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div><!-- end section -->
            
            <div>
            
                <div class="one_half"><img src="/images/riverflow-ps-720x520-3.jpg" alt="" /></div>
                
                <div class="one_half last">
                
                    <h3 class="color">Works of art. Now a lot less work.</h3>
                    
                    
                    <p>Design original illustrations, transform images into paintings, or create your own digital masterpiece. Make 3D artwork that you can animate or print. Enhance your work with unique patterns and eye-catching effects. Paint with smooth lines and curves using advanced brushes that work as fast as you can think.</p>
                    
                </div>
                
                <div class="clearfix margin_top2"></div>
                     
            </div><!-- end section -->
               
              
        </div>

</div>
</div><!-- end features section 59 -->

<div class="clearfix margin_top3"></div>

<div class="features_sec12 two">

    <div class="stcode_title8">

<h2><span class="line"></span><span class="text">What's New</span></h2>

</div>

<div class="clearfix margin_top6"></div>

<div class="container">

      
    
   <div class="one_fourth">
        
            <div class="box">
                <img src="/images/wncd-ps-416x207-1.jpg" alt="">
                <h5>Brush organization and performance</h5>
                <p>Organize and save your brushes in the order you want. And with quicker reaction time, lagging brushes won’t slow you down.</p>
                <br>
                
            </div>
            
        </div><!-- end section -->
    

    
        <div class="one_fourth">
        
            <div class="box">
                <img src="/images/wncd-ps-416x207-2.jpg" alt="">
                <h5>Curvature Pen tool</h5>
                <p>Create paths more quickly and intuitively. Just like in Adobe Illustrator CC, the new Curvature Pen tool enables you to push and pull segments directly.</p>
                <br>
               
            </div>
            
        </div><!-- end section -->
        
        <div class="one_fourth">
        
            <div class="box">
                <img src="/images/wncd-ps-416x207-3.jpg" alt="">
                <h5>Brush stroke smoothing</h5>
                <p>Get a polished look faster. Vary the amount of smoothing for cleaner lines and curves, even when using a mouse.</p>
                <br>
               
            </div>
            
        </div><!-- end section -->
        
        <div class="one_fourth last">
        
            <div class="box">
                <img src="/images/wncd-ps-416x207-4.jpg" alt="">
                <h5>Lightroom photos in Start</h5>
                <p>Your photos are everywhere you are. Access them from Lightroom Cloud Services right inside Photoshop via Search or the Start screen.</p>
                <br>
                
            </div>
            

        
    </div>
    
</div>
</div>

<div class="features_sec53">
<div class="container">

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">New features</span></h2>

</div>

<div class="clearfix margin_top5"></div>

<div class="one_half">
<div class="box">


<h5 class="light">Photoshop CC</h5>

<ul class="list1"> 



<li><i class="fa fa-caret-right"></i> Better brush organization</li>
<li><i class="fa fa-caret-right"></i> Brush performance improvements
</li>
<li><i class="fa fa-caret-right"></i> Access Lightroom Photos
</li>
<li><i class="fa fa-caret-right"></i> Brush stroke smoothing</li>
<li><i class="fa fa-caret-right"></i> Variable fonts
</li>
<li><i class="fa fa-caret-right"></i> Quick Share menu</li>
<li><i class="fa fa-caret-right"></i> Curvature Pen Tool</li>

<li><i class="fa fa-caret-right"></i> Path improvements</li>
<li><i class="fa fa-caret-right"></i> Copy and paste layers
</li>

</ul>

</div>
</div><!-- end section -->



<div class="one_half last">
<div class="box">


<h5 class="light">More Features</h5>

<ul class="list1">       
<li><i class="fa fa-caret-right"></i> Enhanced tooltips</li>
<li><i class="fa fa-caret-right"></i> 360 panorama workflow</li>


<li><i class="fa fa-caret-right"></i> Properties panel improvements</li>
<li><i class="fa fa-caret-right"></i> Support for Microsoft Dial
</li>
<li><i class="fa fa-caret-right"></i> Paste as plain text</li>
<li><i class="fa fa-caret-right"></i> Support for HEIF</li>
<li><i class="fa fa-caret-right"></i> Select and Mask improvements</li>
<li><i class="fa fa-caret-right"></i> General performance improvements
</li>
<li><i class="fa fa-caret-right"></i> And so much more</li>


</ul>

</div>
</div><!-- end section -->



</div>

</div>
<div class="clearfix"></div>


<div class="features_sec4">
<div class="container">

	<div class="onecol_sixty">
    
    	<h3 class="unline"> System Requirments</h3>
    	
<ul class="list1">       
<li><i class="fa fa-caret-right"></i> Windows 10, 8.1 or Windows 7, in 32-bit or 64-bit, all with latest Updates and Service Pack</li>
<li><i class="fa fa-caret-right"></i> Intel Core i3/5/7 or AMD Athlon 64            </li>
<li><i class="fa fa-caret-right"></i> 2 GB RAM          </li>
<li><i class="fa fa-caret-right"></i> 1 GB hard disk space
</li>
<li><i class="fa fa-caret-right"></i> Multi-touch screen, mouse or tablet
</li>
<li><i class="fa fa-caret-right"></i> 1280 x 720 screen resolution at 100% (96 dpi)
</li>
<li><i class="fa fa-caret-right"></i> Microsoft Internet Explorer 11 or higher
</li>
<li><i class="fa fa-caret-right"></i> Microsoft .Net Framework 4.6
</li>
<li><i class="fa fa-caret-right"></i> DVD drive optional (for box installation)
</li>

</ul>
  
    </div><!-- end all sections -->

    <div class="onecol_forty last">
    
		<div class="peosays">
        
            <h3 class="unline"> What People Says</h3>
            
            <div class="clearfix"></div>
            
            <div id="owl-demo11" class="owl-carousel small four">
                
            	<div class="box">
                
                	<div><h6>Name <em>www.websitenames.com</em></h6></div>
                    
                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy generators on the versions have evolved over the years.</p>
                    
                    <span> Rating: &nbsp; <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>
                    
                </div><!-- end slide -->
                
                <div class="box">
                
                	<div><h6>Name 2 <em>www.websitenames.com</em></h6></div>
                    
                    <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy generators on the versions have evolved over the years.</p>
                    
                    <span> Rating: &nbsp; <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </span>
                    
                </div><!-- end slide -->
                
                
            </div>
        
        </div>

     </div>

</div>
</div>


<div class="clearfix"></div>

<div class="features_sec5">

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Support</span></h2>

</div>
<div class="clearfix margin_top6"></div>
<div class="container">
    
    <div class="onecol_sixty">
    
        
        <h5>Setu Technologies have customer satisfaction as our only inspiration. An official Value Added partner, we aim at delivering you the very best, always, officially!!! We are committed in providing the highest quality of service and support to our valued customers so that you can make the most of your software experience. Get in touch by chat, email or phone for product support.</h5>
        
        <ul class="list1">
            <li><i class="fa fa-caret-right"></i>Free Installation of the software at the time of purchase. Our person will come to your office and will do the installation OR Remote installations are also supported if required by clients. (Subjected to your windows is properly updated) </li>
             <li><i class="fa fa-caret-right"></i>We provide Online and Telephonic support during office Hours to all our clients. (Remote Access using AnyDesk & Ammyy Admin)</li>
              <li><i class="fa fa-caret-right"></i>We also involve Corel Technical Team for any 2nd level of Support if approved.</li>
               <li><i class="fa fa-caret-right"></i>We still provide basic technical support to all our existing customers even if the maintenance gets expired.</li>
                <li><i class="fa fa-caret-right"></i>We also help customers to Upgrade their products on timely basis once launched.</li>


        </ul>
       
    
    </div><!-- end section -->
    
    <div class="onecol_forty last animate fadeInRight" data-anim-type="fadeInRight" data-anim-delay="300">
    
        <img src="/images/setutech-support.jpg" alt="" class="rimg">
    
    </div><!-- end section -->
    
    <div class="clearfix margin_top7"></div>
    
   
</div>
</div>
<div class="clearfix"></div>

<div class="content_fullwidth less">


<div class="clearfix marb10"></div>

<div class="stcode_title8">

<h2><span class="line"></span><span class="text">Request a Quote</span></h2>

</div>
<div class="clearfix marb5"></div>

<div class="container">

      <div class="one_full">
   
        
        <div class="cforms">
        
        <form class="sky-form2">
         
          <fieldset>
            <div class="row">
              <section class="col col-6">
                <label class="label">Name</label>
                <label class="input"> <i class="icon-append icon-user"></i>
                  <input type="text" name="name" id="name">
                </label>
              </section>
              <section class="col col-6">
                <label class="label">E-mail</label>
                <label class="input"> <i class="icon-append icon-envelope-alt"></i>
                  <input type="email" name="email" id="email">
                </label>
              </section>
               <section class="col col-6">
                <label class="label">Mobile</label>
                <label class="input"> <i class="icon-append icon-phone"></i>
                  <input type="number" name="mobile" id="mobile">
                    <input type="hidden" value="<?php echo $product ?>" name="productName" id="productName">
                </label>
              </section>
            </div>
           
            <section>
              <label class="label">Message</label>
              <label class="textarea"> <i class="icon-append icon-comment"></i>
                <textarea rows="4" name="message" id="message"></textarea>
              </label>
            </section>
            
          </fieldset>
             <p id="showError" class="showerror"></p>
          <footer>
            <button type="button" id="submit" class="button">Request Quote</button>
          </footer>
          <div class="message"> <i class="icon-ok"></i>
            <p>Your message was successfully sent!</p>
          </div>
        </form>
        
        </div>
        
      </div><!-- end section -->
      


</div>
</div>

<div class="clearfix"></div>

<?php include '../includes/footer.php' ?>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->


</div>


<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->

<script src="../inquiry.js"></script>


<!-- scroll up -->
<script src="../js/scrolltotop/totop.js" type="text/javascript"></script>


<!-- owl carousel -->
<script src="../../js/carouselowl/owl.carousel.js"></script>
<script src="../../js/carouselowl/custom.js"></script>



<script>
$(document).ready(function(){
    $("#click").click(function(){
       $("html, body").animate({ scrollTop: $(document).height() }, "slow");
    });
});
</script>

<script type="text/javascript" src="../js/universal/custom.js"></script>


<?php include '../includes/menujs.php' ?>


</body>
</html>


